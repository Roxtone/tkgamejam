﻿using UnityEngine;

public class ExplosionElementFinishedState : StateMachineBehaviour
{
    void OnStateEnter(Animator animator)
    {
        animator.GetComponent<ExplosionElement>().Finish();
    }
}