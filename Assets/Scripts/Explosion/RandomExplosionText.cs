﻿using UnityEngine;
using UnityEngine.UI;

public class RandomExplosionText : MonoBehaviour
{
    private Text text;

    void Awake()
    {
        text = GetComponent<Text>();
        text.text = ExplosionMessages.Messages[Random.Range(0, ExplosionMessages.Messages.Length)];
    }
}