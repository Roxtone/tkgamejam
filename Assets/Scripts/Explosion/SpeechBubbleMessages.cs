﻿using UnityEngine;

public class SpeechBubbleMessages : MonoBehaviour
{
    public static readonly string[] Messages = new string[]
    {
        "My turn!",
        "I'm up!",
        "Switch!",
        "I got it!!",
        "Ready!"
    };
}