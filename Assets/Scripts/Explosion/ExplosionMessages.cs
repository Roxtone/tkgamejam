﻿public static class ExplosionMessages
{
    public static readonly string[] Messages = new string[]
    {
        "Kaboom!",
        "Boom!",
        "Bang!",
        "Pow!",
        "Yeboot!"
    };
}