﻿using UnityEngine;

public class ExplosionElement : MonoBehaviour
{
    [SerializeField]
    private bool hideOnFinish;

    private ChainExplosion chainExplosion;

    public void Finish()
    {
        chainExplosion.Unregister(this);
        if(hideOnFinish)
        {
            gameObject.SetActive(false);
        }
    }

    void Start()
    {
        chainExplosion = GetComponentInParent<ChainExplosion>();
        chainExplosion.Register(this);
    }
}