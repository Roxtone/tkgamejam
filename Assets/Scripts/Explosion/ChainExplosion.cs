﻿using System.Collections.Generic;
using UnityEngine;

public class ChainExplosion : MonoBehaviour
{
    private List<ExplosionElement> elements;

    public void Register(ExplosionElement element)
    {
        elements.Add(element);
    }

    public void Unregister(ExplosionElement element)
    {
        elements.Remove(element);
    }

    void Awake()
    {
        elements = new List<ExplosionElement>();
    }

    void Update()
    {
        if (elements.Count == 0)
        {
            Finish();
        }
    }

    private void Finish()
    {
        Destroy(gameObject);
    }
}