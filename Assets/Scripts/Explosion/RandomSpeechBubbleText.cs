﻿using UnityEngine;
using UnityEngine.UI;

public class RandomSpeechBubbleText : MonoBehaviour
{
    private Text text;

    void Awake()
    {
        text = GetComponent<Text>();
        text.text = SpeechBubbleMessages.Messages[Random.Range(0, SpeechBubbleMessages.Messages.Length)];
    }
}