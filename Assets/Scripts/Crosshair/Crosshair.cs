﻿using UnityEngine;

public class Crosshair : MonoBehaviour
{
    [SerializeField]
    private float minRange;
    [SerializeField]
    private float maxRange;
    [SerializeField]
    private float sensitivity;

    public static Crosshair Instance { get; private set; }

    public Vector2 Position { get { return transform.position; } }

    public void Move(float xAxis, float yAxis)
    {
        Vector3 movement = new Vector3(xAxis, yAxis, 0) * sensitivity * Time.deltaTime;
        transform.position += movement;
        ApplyRange();
    }

    public void MoveTo(Vector2 position)
    {
        transform.position = new Vector3(position.x, position.y, 0);
        ApplyRange();
    }

    public void ApplyRange()
    {
        float distanceFromCenter = transform.position.magnitude;
        if(distanceFromCenter < minRange)
        {
            transform.position = transform.position.normalized * minRange;
        }
        if(distanceFromCenter > maxRange)
        {
            transform.position = transform.position.normalized * maxRange;
        }
    }

    void Awake()
    {
        Instance = this;
    }
}