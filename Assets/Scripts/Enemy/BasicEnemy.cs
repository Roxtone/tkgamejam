﻿using UnityEngine;

public class BasicEnemy : Enemy
{
    [SerializeField]
    private int damageOnContact;

    private Player target;

    void Awake()
    {
        FindTarget();
        FaceTarget();
    }

    void Update()
    {
        Vector2 toTarget = target.transform.position - transform.position;
        transform.position += (Vector3)(toTarget.normalized * speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Platform platform = other.gameObject.GetComponent<Platform>();
        if(platform != null)
        {
            PlayerHealthController.Instance.Damage(damageOnContact);
            Die();
        }
    }

    protected void FindTarget()
    {
        Player[] players = PlayerSwapController.Instance.Players;
        target = players[Random.Range(0, players.Length)];
    }

    protected void FaceTarget()
    {
        Vector2 targetDirection = target.transform.position - transform.position;
        float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}