﻿using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField]
    protected float speed;
    [SerializeField]
    protected GameObject explosionPrefab;

    public virtual void Die()
    {
        AudioController.Instance.PlayEnemyExplosionClip();
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        ScreenshakeController.Instance.Intensify();
        Destroy(gameObject);
    }
}