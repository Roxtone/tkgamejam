﻿using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    [SerializeField]
    private string xAxisName;
    [SerializeField]
    private string yAxisName;
    [SerializeField]
    private string shootAxisName;

    private PlayerActions playerActions;

    void Awake()
    {
        playerActions = GetComponent<PlayerActions>();
    }

    void Update()
    {
        if(!GameController.Instance.IsGameOver)
        {
            float xAxis = Input.GetAxis(xAxisName);
            float yAxis = Input.GetAxis(yAxisName);

            playerActions.HandleMovement(xAxis, yAxis);

            float shootAxis = Input.GetAxis(shootAxisName);
            if (shootAxis > 0.0f)
            {
                playerActions.HandleShooting();
            }
        }
    }
}
