﻿using UnityEngine;

public class PlatformRotation : MonoBehaviour
{
    private Player player;

    void Awake()
    {
        player = GetComponentInParent<Player>();
    }

    void Update()
    {
        if(PlayerSwapController.Instance.IsOnline(player))
        {
            Rotate();
        }
    }

    private void Rotate()
    {
        Vector2 crosshairPosition = Crosshair.Instance.Position;
        Vector2 targetDirection = crosshairPosition - (Vector2)transform.position;
        float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}