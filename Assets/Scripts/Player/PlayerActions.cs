﻿using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    private Player player;
    private Tower tower;

    public void HandleMovement(float xAxis, float yAxis)
    {
        if(PlayerSwapController.Instance.IsOnline(player))
        {
            Crosshair.Instance.Move(xAxis, yAxis);
        }
    }

    public void HandleShooting()
    {
        if (PlayerSwapController.Instance.IsOnline(player))
        {
            tower.TryToShoot();
        }
    }

    void Awake()
    {
        player = GetComponent<Player>();
        tower = GetComponentInChildren<Tower>();
    }
}