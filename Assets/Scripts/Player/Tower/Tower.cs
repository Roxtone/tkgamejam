﻿using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField]
    private Transform projectileSpawnPoint;

    private TowerAmmo towerAmmo;
    private PlayerAnimationController playerAnimationController;
    private float timeSinceLastShot;

    public void TryToShoot()
    {
        if(timeSinceLastShot > towerAmmo.Ammo.ShootInterval)
        {
            Shoot();
        }
    }

    void Awake()
    {
        towerAmmo = GetComponent<TowerAmmo>();
        playerAnimationController = GetComponentInParent<PlayerAnimationController>();
        timeSinceLastShot = 0.0f;
    }

    void Update()
    {
        timeSinceLastShot += Time.deltaTime;
    }

    private void Shoot()
    {
        playerAnimationController.Shoot();
        timeSinceLastShot = 0.0f;
        GameObject spawnedGameObject = (GameObject)Instantiate(towerAmmo.Ammo.gameObject, projectileSpawnPoint.position, projectileSpawnPoint.rotation);
        Ammo spawnedProjectile = spawnedGameObject.GetComponent<Ammo>();
        spawnedProjectile.Shoot(Crosshair.Instance.Position);
        AudioController.Instance.PlayShotClip();
    }
}