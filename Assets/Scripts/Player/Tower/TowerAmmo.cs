﻿using UnityEngine;

public class TowerAmmo : MonoBehaviour
{
    [SerializeField]
    private Ammo defaultAmmoPrefab;

    public Ammo Ammo { get { return defaultAmmoPrefab; } }
}