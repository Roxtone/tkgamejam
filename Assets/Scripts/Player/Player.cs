﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private string playerName;

    private PlayerAnimationController playerAnimationController;

    public bool IsOnline
    {
        get
        {
            return PlayerSwapController.Instance.IsOnline(this);
        }
        set
        {
            if (value)
            {
                playerAnimationController.FadeIn();
            }
            else
            {
                playerAnimationController.FadeOut();
            }
        }
    }

    void Awake()
    {
        playerAnimationController = GetComponent<PlayerAnimationController>();
    }
}