﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Subwave
{
    [SerializeField]
    private float delay;
    [SerializeField]
    EnemyInfo[] enemyInfos;

    public float Delay { get { return delay; } }

    public Enemy[] Enemies
    {
        get
        {
            List<Enemy> enemies = new List<Enemy>();
            foreach (EnemyInfo enemyInfo in enemyInfos)
            {
                int count = Random.Range(enemyInfo.minCount, enemyInfo.maxCount);
                for (int i = 0; i < count; i++)
                {
                    enemies.Add(enemyInfo.enemyPrefab);
                }
            }
            return enemies.ToArray();
        }
    }

    [System.Serializable]
    public struct EnemyInfo
    {
        [SerializeField]
        public int minCount;
        [SerializeField]
        public int maxCount;
        [SerializeField]
        public Enemy enemyPrefab;
    }
}