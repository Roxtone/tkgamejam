﻿using UnityEngine;

public class Wave : MonoBehaviour
{
    [SerializeField]
    private Subwave[] subwaves;

    public Subwave[] Subwaves { get { return subwaves; } }
}