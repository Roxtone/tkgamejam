﻿using UnityEngine;

public class MusicController : MonoBehaviour
{
    private AudioSource audioSource;
    private float originalVolume;

    public static MusicController Instance { get; private set; }

    public void FadeOut()
    {
        audioSource.volume = 0.0f;
    }

    public void FadeIn()
    {
        audioSource.volume = originalVolume;
    }

    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            audioSource = GetComponent<AudioSource>();
            originalVolume = audioSource.volume;
        }
    }
}