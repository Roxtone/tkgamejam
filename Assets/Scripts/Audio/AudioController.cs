﻿using UnityEngine;

public class AudioController : MonoBehaviour
{
    [SerializeField]
    private AudioClip shotClip;
    [SerializeField]
    private AudioClip ammoExplosionClip;
    [SerializeField]
    private AudioClip enemyExplosionClip;
    [SerializeField]
    private AudioClip playerExplosionClip;
    [SerializeField]
    private AudioClip winClip;
    [SerializeField]
    private AudioClip lossClip;
    [SerializeField]
    private AudioClip countdownClip;

    private AudioSource audioSource;

    public static AudioController Instance { get; private set; }

    public void PlayShotClip()
    {
        PlayClip(shotClip);
    }

    public void PlayAmmoExplosionClip()
    {
        PlayClip(ammoExplosionClip);
    }

    public void PlayEnemyExplosionClip()
    {
        PlayClip(enemyExplosionClip);
    }

    public void PlayPlayerExplosionClip()
    {
        PlayClip(playerExplosionClip);
    }

    public void PlayWinClip()
    {
        PlayClip(winClip);
    }

    public void PlayLossClip()
    {
        PlayClip(lossClip);
    }

    public void PlayCountdownClip()
    {
        PlayClip(countdownClip);
    }

    void Awake()
    {
        Instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    private void PlayClip(AudioClip audioClip)
    {
        audioSource.PlayOneShot(audioClip);
    }
}