﻿using UnityEngine;

public abstract class Ammo : MonoBehaviour
{
    [SerializeField]
    private float shootInterval;
    [SerializeField]
    protected float projectileSpeed;
    [SerializeField]
    protected float destroyAtProximity;
    [SerializeField]
    protected GameObject explosionPrefab;

    protected Vector2 target;

    public float ShootInterval { get { return shootInterval; } }

    public virtual void Shoot(Vector2 target)
    {
        this.target = target;
    }

    protected virtual void Update()
    {
        MoveToTarget();
    }

    protected virtual void MoveToTarget()
    {
        Vector2 toTarget = target - (Vector2)transform.position;
        if (toTarget.magnitude > destroyAtProximity)
        {
            transform.position += (Vector3)(toTarget.normalized * projectileSpeed * Time.deltaTime);
        }
        else
        {
            Explode();
        }
    }

    protected virtual void Explode()
    {
        AudioController.Instance.PlayAmmoExplosionClip();
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        ScreenshakeController.Instance.Intensify();
        Destroy(gameObject);
    }
}