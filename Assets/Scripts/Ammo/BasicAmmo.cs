﻿using UnityEngine;

public class BasicAmmo : Ammo
{
    void OnTriggerEnter2D(Collider2D other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.Die();
            Explode();
        }
    }
}