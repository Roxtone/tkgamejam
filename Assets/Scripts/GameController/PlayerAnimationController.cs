﻿using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    private static readonly string fadeInParameter = "FadeIn";
    private static readonly string fadeOutParameter = "FadeOut";
    private static readonly string shootParameter = "Shoot";

    public void FadeIn()
    {
        animator.SetTrigger(fadeInParameter);
    }

    public void FadeOut()
    {
        animator.SetTrigger(fadeOutParameter);
    }

    public void Shoot()
    {
        animator.SetTrigger(shootParameter);
    }
}