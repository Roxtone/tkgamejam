﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class WaveSpawnerController : MonoBehaviour
{
    [SerializeField]
    private Wave[] waves;
    [SerializeField]
    private float spawnRadius;

    private Enemy[] enemiesToKill;

    void Awake()
    {
        StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        for (int currentWaveIndex = 0; currentWaveIndex < waves.Length; currentWaveIndex++)
        {
            for (int currentSubwaveIndex = 0; currentSubwaveIndex < waves[currentWaveIndex].Subwaves.Length; currentSubwaveIndex++)
            {
                Subwave currentSubwave = waves[currentWaveIndex].Subwaves[currentSubwaveIndex];
                yield return new WaitForSeconds(currentSubwave.Delay);
                SpawnSubwave(currentSubwave);
                while(!IsSubwaveCleared())
                {
                    yield return null;
                }
            }
        }
        GameController.Instance.Win();
    }

    private void SpawnSubwave(Subwave subwave)
    {
        Enemy[] enemiesToSpawn = subwave.Enemies;
        enemiesToKill = new Enemy[enemiesToSpawn.Length];
        for(int i = 0; i < enemiesToSpawn.Length; i++)
        {
            enemiesToKill[i] = SpawnEnemy(enemiesToSpawn[i]);
        }
    }

    private Enemy SpawnEnemy(Enemy enemyPrefab)
    {
        int randomAngle = Random.Range(0, 360);
        Vector2 spawnPosition = Quaternion.Euler(0, 0, randomAngle) * (Vector2.right * spawnRadius);
        GameObject enemyGameObject = (GameObject)Instantiate(enemyPrefab.gameObject, spawnPosition, Quaternion.identity);
        return enemyGameObject.GetComponent<Enemy>();
    }

    private bool IsSubwaveCleared()
    {
        return enemiesToKill.All(e => e == null);
    }
}