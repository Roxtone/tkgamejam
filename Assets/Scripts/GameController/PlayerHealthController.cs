﻿using UnityEngine;

public class PlayerHealthController : MonoBehaviour
{
    [SerializeField]
    private int startingHealth;
    [SerializeField]
    private GameObject explosionPrefab;

    public static PlayerHealthController Instance { get; private set; }

    public int Health { get; private set; }

    public float HealthPercentage { get { return Mathf.Clamp(Health / startingHealth, 0.0f, 1.0f); } }

    public void Damage(int damage)
    {
        Health = Mathf.Max(Health - damage, 0);
        if(Health == 0)
        {
            AudioController.Instance.PlayPlayerExplosionClip();
            Instantiate(explosionPrefab);
            GameController.Instance.Lose();
        }
    }
    
    void Awake()
    {
        Instance = this;
        Health = startingHealth;
    }
}