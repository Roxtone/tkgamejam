﻿using System.Collections;
using UnityEngine;

public class PlayerSwapController : MonoBehaviour
{
    [SerializeField]
    private Player playerOne;
    [SerializeField]
    private Player playerTwo;
    [SerializeField]
    private float timeToSwap;
    [SerializeField]
    private GameObject speechBubbleExplosion;
    [SerializeField]
    private bool debugOnePlayerMode;

    private Player onlinePlayer;
    private bool swapSoundPlayed;

    public static PlayerSwapController Instance { get; private set; }

    public float CurrentTimeToSwap { get; private set; }

    public Player[] Players { get { return new Player[] { playerOne, playerTwo }; } }

    public bool IsOnline(Player player)
    {
        return player == onlinePlayer;
    }

    void Awake()
    {
        Instance = this;
        swapSoundPlayed = true;
    }

    void Start()
    {
        Swap();
    }

    void Update()
    {
        if(!GameController.Instance.IsGameOver)
        {
            CurrentTimeToSwap -= Time.deltaTime;
            if (CurrentTimeToSwap < 0.0f)
            {
                Swap();
            }
            else if (CurrentTimeToSwap < 3.0f && !swapSoundPlayed)
            {
                AudioController.Instance.PlayCountdownClip();
                swapSoundPlayed = true;
            }
        }
    }

    private void Swap()
    {
        CurrentTimeToSwap = timeToSwap;
        if (onlinePlayer == null)
        {
            onlinePlayer = playerOne;
            playerOne.IsOnline = true;
        }
        else
        {
            if (!debugOnePlayerMode)
            {
                if (onlinePlayer == playerOne)
                {
                    onlinePlayer = playerTwo;
                    playerOne.IsOnline = false;
                    playerTwo.IsOnline = true;
                }
                else
                {
                    onlinePlayer = playerOne;
                    playerOne.IsOnline = true;
                    playerTwo.IsOnline = false;
                }
            }
        }
        Instantiate(speechBubbleExplosion, onlinePlayer.transform.position, Quaternion.identity);
        swapSoundPlayed = false;
    }
}