﻿using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    public bool IsGameOver { get; private set; }

    public void Win()
    {
        if(!IsGameOver)
        {
            MusicController.Instance.FadeOut();
            AudioController.Instance.PlayWinClip();
            GameOverScreen.Instance.Show(GameOverScreen.WinMessage);
            IsGameOver = true;
        }
    }

    public void Lose()
    {
        if (!IsGameOver)
        {
            MusicController.Instance.FadeOut();
            AudioController.Instance.PlayLossClip();
            GameOverScreen.Instance.Show(GameOverScreen.LossMessage);
            IsGameOver = true;
        }
    }

    void Awake()
    {
        Instance = this;
    }
}