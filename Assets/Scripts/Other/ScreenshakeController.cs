﻿using UnityEngine;

public class ScreenshakeController : MonoBehaviour
{
    [SerializeField]
    private float intensification;
    [SerializeField]
    private float decrease;

    private Vector3 originalPosition;
    private float currentShakeIntensity;

    public static ScreenshakeController Instance { get; private set; }

    public void Intensify()
    {
        currentShakeIntensity += intensification;
    }

    void Awake()
    {
        Instance = this;
        originalPosition = transform.position;
        currentShakeIntensity = 0.0f;
    }

    void Update()
    {
        transform.position = originalPosition + (Vector3)(Random.insideUnitCircle * currentShakeIntensity);
        currentShakeIntensity = Mathf.Max(currentShakeIntensity - decrease * Time.deltaTime, 0.0f);
    }
}