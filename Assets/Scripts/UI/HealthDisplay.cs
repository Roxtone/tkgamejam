﻿using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : UIBehaviour
{
    [SerializeField]
    private LayoutElement heartPrefab;

    void Start()
    {
        int maxHealth = PlayerHealthController.Instance.Health;
        SpawnHearts(maxHealth);
    }

    void Update()
    {
        int currentHealth = PlayerHealthController.Instance.Health;
        int heartsToLose = transform.childCount - currentHealth;
        for (int i = 0; i < heartsToLose; i++)
        {
            Destroy(FirstChild.gameObject);
        }
    }

    private void SpawnHearts(int count)
    {
        for (int i = 0; i < count; i++)
        {
            LayoutElement heart = Instantiate(heartPrefab);
            heart.transform.SetParent(transform);
        }
    }
}