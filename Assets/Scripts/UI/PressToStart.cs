﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PressToStart : UIBehaviour
{
    void Update()
    {
        if(Input.anyKeyDown)
        {
            SceneManager.LoadScene((int)Scenes.Game);
        }
    }
}