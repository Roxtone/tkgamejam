﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : UIBehaviour
{
    public static readonly string WinMessage = "All waves cleared!";
    public static readonly string LossMessage = "You're dead!";

    [SerializeField]
    private float showDelay;
    [SerializeField]
    private Text messageText;

    private bool isVisible;

    public static GameOverScreen Instance { get; private set; }

    public void Show(string message)
    {
        messageText.text = message;
        StartCoroutine(ShowCoroutine());
    }

    void Awake()
    {
        Instance = this;
        isVisible = false;
    }

    void Update()
    {
        if (isVisible && Input.anyKeyDown)
        {
            MusicController.Instance.FadeIn();
            SceneManager.LoadScene((int)Scenes.MainMenu);
        }
    }

    private IEnumerator ShowCoroutine()
    {
        yield return new WaitForSeconds(showDelay);
        FirstChild.gameObject.SetActive(true);
        isVisible = true;
    }
}