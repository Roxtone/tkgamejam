﻿using UnityEngine;
using UnityEngine.UI;

public class MovieOverlay : MonoBehaviour
{
    [SerializeField]
    private float positionShakeIntensity;
    [SerializeField]
    private float colorShakeIntensity;

    private RectTransform rectTransform;
    private Image image;
    private Vector2 originalPosition;
    private Color originalColor;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        image = GetComponent<Image>();
        originalPosition = rectTransform.anchoredPosition;
        originalColor = image.color;
    }

    void Update()
    {
        rectTransform.anchoredPosition = originalPosition + Random.insideUnitCircle * positionShakeIntensity;
        image.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a + Random.value * colorShakeIntensity);
    }
}