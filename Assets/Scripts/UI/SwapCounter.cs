﻿using UnityEngine;
using UnityEngine.UI;

public class SwapCounter : UIBehaviour
{
    private Text text;

    private string originalText;

    void Awake()
    {
        text = GetComponent<Text>();
        originalText = text.text;
    }

    void Update()
    {
        text.text = string.Format(originalText, (int)PlayerSwapController.Instance.CurrentTimeToSwap);
    }
}